package com.example.aaronkalsi.video_game_dictionary.Models;

//Created by Aaron Kalsi on 12/03/2018.

//Game POJO class used to convert JSON string data into objects.

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Game implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("hypes")
    @Expose
    private int hypes;
    @SerializedName("rating")
    @Expose
    private double rating;
    @SerializedName("popularity")
    @Expose
    private float popularity;
    @SerializedName("first_release_date")
    @Expose
    private long firstReleaseDate;
    @SerializedName("screenshots")
    @Expose
    private List<Screenshots> screenshots = null;
    @SerializedName("cover")
    @Expose
    private Cover cover;
    @SerializedName("pegi")
    @Expose
    private Pegi pegi;
    @SerializedName("coverURL")
    @Expose
    private String coverURL;
    @SerializedName("pegiInfo")
    @Expose
    private String pegiInfo;

    public Game(int id, String name, String url, String summary, int hypes, double rating, float popularity, long firstReleaseDate, String coverURL, String pegiInfo) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.summary = summary;
        this.hypes = hypes;
        this.rating = rating;
        this.popularity = popularity;
        this.firstReleaseDate = firstReleaseDate;
        this.coverURL  = coverURL;
        this.pegiInfo = pegiInfo;
    }

    public Game(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getHypes() {
        return hypes;
    }

    public void setHypes(int hypes) {
        this.hypes = hypes;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public long getFirstReleaseDate() {
        return firstReleaseDate;
    }

    public void setFirstReleaseDate(long firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }

    public List<Screenshots> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(List<Screenshots> screenshots) {
        this.screenshots = screenshots;
    }

    public Cover getCover() { return cover; }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public Pegi getPegi() {
        return pegi;
    }

    public void setPegi(Pegi pegi) {
        this.pegi = pegi;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public String getPegiInfo() {
        return pegiInfo;
    }

    public void setPegiInfo(String pegiInfo) {
        this.pegiInfo = pegiInfo;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public Game (Parcel in) {
        int[] gamedata1 = new int[2];
        String[] gamedata2 = new String[5];
        double[] gamedata3 = new double[1];
        float[] gamedata4 = new float[1];
        long[] gamedata5 = new long[1];
        //ArrayList<Screenshots> gamedata6 = new ArrayList<Screenshots>();
        //ArrayList<Cover> gamedata7 = new ArrayList<Cover>();
        //cover gamedata7 = new Cover();
        //Pegi gamedata8 = new Pegi();

        in.readIntArray(gamedata1);
        this.id = gamedata1[0];
        this.hypes = gamedata1[1];

        in.readStringArray(gamedata2);
        this.name = gamedata2[0];
        this.url = gamedata2[1];
        this.summary = gamedata2[2];
        this.coverURL = gamedata2[3];
        this.pegiInfo = gamedata2[4];

        in.readDoubleArray(gamedata3);
        this.rating = gamedata3[0];

        in.readFloatArray(gamedata4);
        this.popularity = gamedata4[0];

        in.readLongArray(gamedata5);
        this.firstReleaseDate = gamedata5[0];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeIntArray(new int[] {this.id, this.hypes});
        dest.writeStringArray(new String[] {this.name, this.url, this.summary, this.coverURL,this.pegiInfo});
        dest.writeDoubleArray(new double[] {this.rating});
        dest.writeFloatArray(new float[] {this.popularity});
        dest.writeLongArray(new long[] {this.firstReleaseDate});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Game createFromParcel (Parcel in) {return new Game(in);}

        public Game[] newArray(int size) {return new Game[size];}
    };

}