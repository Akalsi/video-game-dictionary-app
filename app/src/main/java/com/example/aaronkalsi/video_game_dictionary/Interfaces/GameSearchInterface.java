package com.example.aaronkalsi.video_game_dictionary.Interfaces;

import com.example.aaronkalsi.video_game_dictionary.Models.Game;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

//Created by Aaron Kalsi on 19/03/2018.


public interface GameSearchInterface {

    @Headers({"accept: application/json", "user-key: 34f109011f4a795335f4c81d296a5468"}) //Sets the headers needed for the API Call

    @GET("games/?fields=name,url,summary,popularity,rating,first_release_date,cover,screenshots,pegi,hypes&order=popularity:desc&filter[version_parent][not_exists]=1&filter[cover][exists]&filter[screenshots][exists]") //Calls on the endpoint used for the API call this is added onto the baseURL.

    Call<List<Game>> getSearchGames(@Query("search") String GameSearchQuery);
}
