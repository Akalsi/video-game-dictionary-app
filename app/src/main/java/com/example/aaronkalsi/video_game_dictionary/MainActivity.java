package com.example.aaronkalsi.video_game_dictionary;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button GameBTN; //Button variable
    private Button DeveloperBNT;
    private ImageView AppIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Connects or Links java file with specific layout

        GameBTN = findViewById(R.id.GamesBtn); //Links GameBTN variable in code with
        GameBTN.setOnClickListener(this); //Sets anon click listener to the button
        DeveloperBNT=findViewById(R.id.DeveloperBtn);
        DeveloperBNT.setOnClickListener(this);
        AppIcon = findViewById(R.id.AppLogo);

        Picasso.get().load(R.drawable.app_logo).fit().into(AppIcon);//loads app logo into image view
    }

    @Override
    public void onClick(View view){

        if(view==GameBTN)
        {
            //finish(); //When finish uncommented if back button pressed in view games app closes
            Intent GameIntent = new Intent(MainActivity.this, ViewGames.class); //Creates a new intent saying to move to the ViewGames Class/Layout.
            startActivity(GameIntent); //Activates the intent.

        }

        if (view==DeveloperBNT)
        {
            Intent DeveloperIntent = new Intent(MainActivity.this, ViewDevelopers.class); //Creates a new intent saying to move to the ViewDevelopers Class/Layout.
            startActivity(DeveloperIntent); //Activates the intent.
        }


    }


    @Override
    public void onBackPressed() //Checks if the back button has been pressed
    {
    }

}
