package com.example.aaronkalsi.video_game_dictionary.Models;

//Created by Aaron Kalsi on 15/03/2018.

//Logo POJO class used to convert JSON string data into objects.


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Logo implements Parcelable{

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("cloudinary_id")
    @Expose
    private String cloudinaryId;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCloudinaryId() {
        return cloudinaryId;
    }

    public void setCloudinaryId(String cloudinaryId) {
        this.cloudinaryId = cloudinaryId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public Logo (Parcel in)
    {
        String[] logodata1 = new String[2];
        int[] logodata2 = new int[2];


        in.readStringArray(logodata1);
        this.url = logodata1[0];
        this.cloudinaryId = logodata1[1];


        in.readIntArray(logodata2);
        this.width = logodata2[0];
        this.height = logodata2[1];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeStringArray(new String[] {this.url, this.cloudinaryId});
        dest.writeIntArray(new int[] {this.width, this.height});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Logo createFromParcel (Parcel in) {return new Logo(in);}

        public Logo[] newArray(int size) {return new Logo[size];}
    };

}