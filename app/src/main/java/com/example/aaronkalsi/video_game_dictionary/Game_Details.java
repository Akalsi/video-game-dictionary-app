package com.example.aaronkalsi.video_game_dictionary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aaronkalsi.video_game_dictionary.Models.Cover;
import com.example.aaronkalsi.video_game_dictionary.Models.Game;
import com.example.aaronkalsi.video_game_dictionary.Models.Pegi;
import com.example.aaronkalsi.video_game_dictionary.Models.Screenshots;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Created by Aaron Kalsi on 09/04/2018.



public class Game_Details extends AppCompatActivity implements View.OnClickListener{

    private TextView gameName;
    private ImageView gameCover;
    private TextView gameDescription;
    private TextView gameRating;
    private TextView gameReleaseDate;
    private TextView gamePopularity;
    private TextView gameHype;
    private TextView gamePegi;
    private Button saveGame;
    private String ReleaseDate;
    private String gameRatingValue;
    private String gamePopularityValue;
    private Game game;
    private Pegi pegi;
    private DatabaseReference appDatabase;
    private static final String TAG = "debug";
    private static final String isGameSaved = "MyPrefsFile";
    private String isSavedValue ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_details);

        gameName = findViewById(R.id.gameName);
        gameCover = findViewById(R.id.gameCover);
        gameDescription = findViewById(R.id.gameDescription);
        gameRating = findViewById(R.id.gameRating);
        gameReleaseDate = findViewById(R.id.gameReleaseData);
        gamePopularity = findViewById(R.id.gamePopularity);
        gameHype = findViewById(R.id.gameHypeRating);
        gamePegi = findViewById(R.id.gamePegi);
        saveGame = findViewById(R.id.SaveGameBtn);

        Bundle gamebundle = getIntent().getExtras();
        game = gamebundle.getParcelable("GameData"); //gets game data from previous intent/activity
        pegi = gamebundle.getParcelable("GamePegiData"); //gets game pegi data from previous intent/activity

        saveGame.setOnClickListener(this);

        appDatabase =  FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Games"); //Sets FBDB Reference

        gameCoverLoad();

        displayGameDetails();

        checkSave();
    }
    //Downloads and loads cover image of the selected game into the image view//////////////////////
    private void gameCoverLoad()
    {

        String coverURL= game.getCoverURL();
        Picasso.get().load(coverURL).fit().into(gameCover);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Converts Date from Unix Format into Standard Date Format//////////////////////////////////////
    private void dateConverter()
    {

        long unixdate = game.getFirstReleaseDate()/1000;
        DateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixdate * 1000);
        ReleaseDate = dateformatter.format(calendar.getTime());
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Number formatter to convert values to 2sf/////////////////////////////////////////////////////
    private void numberFormatter()
    {
        NumberFormat numformatter = new DecimalFormat("#0.0");
        gameRatingValue = numformatter.format(game.getRating());
        gamePopularityValue = numformatter.format(game.getPopularity());
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Displays and populates text views with formatted game data////////////////////////////////////
    private void displayGameDetails()
    {
        dateConverter();

        numberFormatter();

        assert game != null; //ensure game object being received from previous key is not null
        gameName.setText(game.getName()); //Sets text view to the name of the game
        gameDescription.setText("Description: " + game.getSummary()); //Sets text view to the summary of the game
        gameRating.setText("Rating: " + gameRatingValue); //Sets text view to the game rating
        gameReleaseDate.setText("Release Date: " + ReleaseDate); //Sets text view to show release date
        gamePopularity.setText("Popularity Hits: " + gamePopularityValue); //Sets text view to popularity of the game
        gameHype.setText("Follower Before Release: " + (String.valueOf(game.getHypes()))); //Sets text view to hype value in data

        final int GameID = game.getId(); //gets game ID of the game
        Log.d(TAG, "GameID: "+GameID);//Log message

        if (pegi != null && pegi.getSynopsis() != null) //Ensures pegi object an pegi synopsis are present
        {
            String pegiInformation = pegi.getSynopsis();
            game.setPegiInfo(pegiInformation);
        }

        if (game.getPegiInfo() != null)
        {

            gamePegi.setText("Age Restrictions: "+game.getPegiInfo()); //sets text view text to synopsis data
        }
        else
        {
            gamePegi.setText("Age Restrictions: No Data Available"); //sets pegi text box text
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Saves game using game bundle data and game constructor to make game save object, also shared preferences used to keep track of if a game is saved or not
    public void saveGames()
    {
        Bundle gamebundle = getIntent().getExtras();
        game = gamebundle.getParcelable("GameData");

        int ID = game.getId();
        String name = game.getName();
        String url = game.getUrl();
        String summary = game.getSummary();
        int hypes = game.getHypes();
        double rating = game.getRating();
        float popularity = game.getPopularity();
        long releaseDate = game.getFirstReleaseDate();
        String coverUrl = game.getCoverURL();
        String pegiInfo = game.getPegiInfo();

        Game savedGame = new Game(ID, name,url,summary,hypes,rating,popularity,releaseDate,coverUrl,pegiInfo);
        appDatabase.child(name + " - " + ID).setValue(savedGame);

        Toast.makeText(this,"Game Saved Successfully",Toast.LENGTH_LONG).show();

        SharedPreferences.Editor editor = getSharedPreferences(isGameSaved, MODE_PRIVATE).edit();
        editor.putString(game.getName(),"Yes");
        editor.apply();

        finish();
        startActivity(getIntent());

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //Checks value of shared preference on activity startup to check if a game has been saved.
    public void checkSave()
    {
        SharedPreferences prefs = getSharedPreferences(isGameSaved, MODE_PRIVATE);
        isSavedValue = prefs.getString(game.getName(),"No"); //sets isSavedValue to shared prefs value

        if (isSavedValue.equals("Yes"))
        {
            saveGame.setBackgroundColor(Color.parseColor("#006400"));
            String RemoveText="Remove Saved Game";
            saveGame.setText(RemoveText);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Creates option menu using the items within items.xml file/////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.items, menu);
        return super.onCreateOptionsMenu(menu);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Checks which menu option has been selected and says what to do////////////////////////////////
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.shareFacebook:
                //Share Dialog and share link content builder created these both work together the share the game URL of the specific game
                //Quote/message not set within the share as this violates FB policy but user can enter message.
                ShareDialog shareDialog = new ShareDialog(this);
                ShareLinkContent content = new ShareLinkContent.Builder()
                        //.setQuote(game.getName() + " is such a great game it has a " + gameRatingValue + " rating check it out!!!")
                        .setContentUrl(Uri.parse(game.getUrl()))
                        .build();

                if(ShareDialog.canShow(ShareLinkContent.class))
                {
                    shareDialog.show(content); //Shows share content being shared
                }

                break;

            case R.id.shareTwitter: //if twitter button pressed compose/show tweet
                TweetComposer.Builder tweet= new TweetComposer.Builder(this).text(gameName.getText().toString().trim() + " is an amazing game with a rating of " + gameRatingValue + " can't believe it was released " + ReleaseDate);
                tweet.show();
                break;

            case R.id.bookmarks: //if bookmarks pressed go to BM page
                Intent intent = new Intent(Game_Details.this, Bookmark_Games.class);
                startActivity(intent);
                break;

        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(View view){

        if (view==saveGame)
        {
            if (isSavedValue.equals("No")) // Checks if when button pressed shared pref value is No then save game
            {
                saveGames();
            }
            else //if SP value is Yes remove saved object
            {
                DatabaseReference appDatabase =FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Games").child(game.getName() + " - " + game.getId());
                appDatabase.removeValue(); //Removes the current game from Fire base DB

                SharedPreferences.Editor editor = getSharedPreferences(isGameSaved, MODE_PRIVATE).edit();
                editor.putString(game.getName(),"No"); //change SP value to no
                editor.apply();

                Toast.makeText(this,"Game Removed",Toast.LENGTH_LONG).show();
                finish();
                startActivity(getIntent());

            }


        }
    }

    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }
}
