package com.example.aaronkalsi.video_game_dictionary.Models;

//Screenshots POJO class used to convert JSON string data into objects.

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Screenshots implements Parcelable{

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("cloudinary_id")
    @Expose
    private String cloudinaryId;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;

    public Screenshots(String url) {

        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCloudinaryId() {
        return cloudinaryId;
    }

    public void setCloudinaryId(String cloudinaryId) {
        this.cloudinaryId = cloudinaryId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public Screenshots (Parcel in)
    {
        String[] SSdata1 = new String[2];
        int[] SSdata2 = new int[2];

        in.readStringArray(SSdata1);
        this.url = SSdata1[0];
        this.cloudinaryId = SSdata1[1];


        in.readIntArray(SSdata2);
        this.width = SSdata2[0];
        this.height = SSdata2[1];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeStringArray(new String[] {this.url, this.cloudinaryId});
        dest.writeIntArray(new int[] {this.width, this.height});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Screenshots createFromParcel (Parcel in) {return new Screenshots(in);}

        public Screenshots[] newArray(int size) {return new Screenshots[size];}
    };


    }