package com.example.aaronkalsi.video_game_dictionary;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.aaronkalsi.video_game_dictionary.Models.Developer;

//Created by Aaron Kalsi on 25/04/2018.

public class View_Site extends AppCompatActivity {

    private static final String TAG = "debug";
    private WebView WV;
    private String url = "https://www.google.com";
    private Developer devweb;
    private Developer devtwitter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        Bundle Bundleforsite = getIntent().getExtras(); //Gets data passed from dev details.
        devweb = Bundleforsite.getParcelable("devinfoweb");
        devtwitter = Bundleforsite.getParcelable("devinfotwitter");

        configWebView();

    }

    private class MyBrowser extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }

    }

    private void configWebView()
    {
        if (devweb != null && devtwitter == null) //Checks that website button was pressed in dev details as devweb would have been passed in
        {
            url=devweb.getWebsite(); //Sets URL to Website link
        }
        else if (devweb == null && devtwitter != null) //Checks that twitter button was pressed in dev details as devtwitter would have been passed in
        {
            url = devtwitter.getTwitter(); //sets url to twitter link
        }

        WV = findViewById(R.id.WebsiteView);
        WV.setWebViewClient(new MyBrowser()); //sets mybrowser class to web view client
        WV.getSettings().setLoadsImagesAutomatically(true); //loads images automatically
        WV.getSettings().setJavaScriptEnabled(true);
        WV.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY); //sets scrolling style
        WV.loadUrl(url); //Sets URL to load
    }

    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }
}
