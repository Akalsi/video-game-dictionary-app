package com.example.aaronkalsi.video_game_dictionary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aaronkalsi.video_game_dictionary.Models.Developer;
import com.example.aaronkalsi.video_game_dictionary.Models.Logo;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//Created by Aaron Kalsi on 09/04/2018.

public class Developer_Details extends AppCompatActivity implements View.OnClickListener{

    private TextView DevName;
    private ImageView DevLogo;
    private TextView DevDescription;
    private TextView DevStartDate;
    private TextView DevWebsite;
    private TextView DevTwitter;
    private String startDate;
    private Button saveDev;
    private Bundle devBundle;
    private Developer dev;
    private DatabaseReference appDatabase;
    private static final String isDevSaved = "MyPrefsFile";
    private String isSavedValue ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.developer_details);

        DevName = findViewById(R.id.devName);
        DevLogo = findViewById(R.id.devLogo);
        DevDescription = findViewById(R.id.devDescription);
        DevStartDate = findViewById(R.id.devStartDate);
        DevWebsite = findViewById(R.id.devWebSite);
        DevTwitter = findViewById(R.id.devTwitter);
        saveDev = findViewById(R.id.SaveDevBtn);

        devBundle = getIntent().getExtras();
        dev = devBundle.getParcelable("DevData"); //Gets dev data from previous activity

        developerLogoLoad();
        displayDeveloperDetails();
        checkSave();

        appDatabase =  FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Developers"); //Sets FBDB reference


        DevWebsite.setOnClickListener(this);
        DevTwitter.setOnClickListener(this);
        saveDev.setOnClickListener(this);



    }

    private void developerLogoLoad()
    {
        String logoURL = dev.getLogoURL();
        Picasso.get().load(logoURL).fit().into(DevLogo);  //Sets Dev Logo
    }

    private void dateConverter()
    {
        //Converts Unix Start Date into Normal Date Format//////////////////////////////////////////
        long unixDate = dev.getStartDate()/1000;
        DateFormat startDateformatter = new SimpleDateFormat("d/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixDate * 1000);
        startDate = startDateformatter.format(calendar.getTime());
        ////////////////////////////////////////////////////////////////////////////////////////////
    }

    //Displays developer details by populating text views with relevant object data/////////////////
    private void displayDeveloperDetails()
    {
        dateConverter();

        assert dev != null;

        DevName.setText(dev.getName());
        DevDescription.setText("Description: " + dev.getDescription());
        DevStartDate.setText("Developer Start Date: " + startDate);
        DevWebsite.setText("Website: " + dev.getWebsite());
        DevTwitter.setText("Twitter: " + dev.getTwitter());
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Saves dev using game bundle data and dev constructor to make game save object, also shared preferences used to keep track of if a dev is saved or not
    private void saveDevs()
    {
        int ID = dev.getId();
        String name = dev.getName();
        String url = dev.getUrl();
        String description = dev.getDescription();
        Long  startDate= dev.getStartDate();
        String website= dev.getWebsite();
        String twitter = dev.getTwitter();
        String logoURL= dev.getLogoURL();

        Developer savedDeveloper = new Developer(ID, name, url, description, startDate, website, twitter, logoURL);
        appDatabase.child(name + " - " + ID).setValue(savedDeveloper);

        Toast.makeText(this,"Developer Saved Successfully",Toast.LENGTH_LONG).show();

        SharedPreferences.Editor editor = getSharedPreferences(isDevSaved, MODE_PRIVATE).edit(); //Creates shared preference
        editor.putString(dev.getName(),"Yes"); //Sets SP
        editor.apply();

        finish();
        startActivity(getIntent());

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Checks value of shared preference on activity startup to check if a game has been saved.//////
    private void checkSave()
    {
        SharedPreferences prefs = getSharedPreferences(isDevSaved, MODE_PRIVATE);
        isSavedValue = prefs.getString(dev.getName(),"No"); //sets isSavedValue to shared prefs value

        if (isSavedValue.equals("Yes"))
        {
            saveDev.setBackgroundColor(Color.parseColor("#006400"));
            String RemoveText="Remove Saved Developer";
            saveDev.setText(RemoveText);
        }

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(View view){

        if(view==DevWebsite)
        {
            Intent intent = new Intent(Developer_Details.this, View_Site.class); //Intent from developer to site view
            intent.putExtra("devinfoweb",dev); //Attaches Dev obj data to the intent
            startActivity(intent); //start intent
        }
        if (view==DevTwitter)
        {
            Intent intent = new Intent(Developer_Details.this, View_Site.class); //Intent from developer to site view
            intent.putExtra("devinfotwitter",dev); //Attaches Dev obj data to the intent
            startActivity(intent);
        }
        if (view==saveDev)
        {
            if (isSavedValue.equals("No")) // Checks if when button pressed shared pref value is No then save dev
            {
                saveDevs();
            }
            else //if SP value is Yes remove saved object
            {
                DatabaseReference appDatabase =FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Developers").child(dev.getName() + " - " + dev.getId());
                appDatabase.removeValue(); //Removes the current dev from Fire base DB

                SharedPreferences.Editor editor = getSharedPreferences(isDevSaved, MODE_PRIVATE).edit();
                editor.putString(dev.getName(),"No"); //change SP value to no
                editor.apply();

                Toast.makeText(this,"Developer Removed",Toast.LENGTH_LONG).show();
                finish();
                startActivity(getIntent());
            }
        }


    }

    //Creates option menu using the items within items.xml file/////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.items, menu);
        return super.onCreateOptionsMenu(menu);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Checks which menu option has been selected and says what to do////////////////////////////////
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.shareFacebook:
                //Share Dialog and share link content builder created these both work together the share the dev website of the specific dev
                //Quote/message not set within the share as this violates FB policy but user can enter message
                ShareDialog shareDialog = new ShareDialog(this);
                ShareLinkContent content = new ShareLinkContent.Builder()
                        //.setQuote(dev.getName() + " are a great group of developers check out their website!!! ")
                        .setContentUrl(Uri.parse(dev.getWebsite()))
                        .build();

                if(ShareDialog.canShow(ShareLinkContent.class))
                {
                    shareDialog.show(content);
                }

                break;

            case R.id.shareTwitter:
                TweetComposer.Builder tweet= new TweetComposer.Builder(this).text(DevName.getText().toString().trim() + " have developed some great games can't believe they started in " + startDate);
                tweet.show(); //If twitter button pressed compose and show tweet
                break;

            case R.id.bookmarks:
                Intent intent = new Intent(Developer_Details.this, Bookmark_Developers.class); //Creates intent to bookmarked devs
                startActivity(intent);
                break;

        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }
}
