package com.example.aaronkalsi.video_game_dictionary;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.aaronkalsi.video_game_dictionary.Interfaces.GameInterface;
import com.example.aaronkalsi.video_game_dictionary.Interfaces.GameSearchInterface;
import com.example.aaronkalsi.video_game_dictionary.Models.Cover;
import com.example.aaronkalsi.video_game_dictionary.Models.Game;
import com.example.aaronkalsi.video_game_dictionary.Models.Screenshots;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Created by Aaron Kalsi on 12/03/2018.


public class ViewGames extends AppCompatActivity{

    private static final String TAG = "debug";
    private ListView gamesList; //ListView Variable
    private Game_Adapter gameAdapter; //game adapter variable
    private SearchView gameSearch; //gameSearch Variable

    List<String> CoverURLS = new ArrayList<>(); //URL for cover images stored within coversURLS List

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_games);
        gamesList = findViewById(R.id.GamesList); //Links the gameList variable with the GamesList in the layout/view.
        gameSearch = findViewById(R.id.GameSearchBar);  //Links the gameSearch variable with the GameSearchBar in the layout/view.

        gameAPICall();
        gameSearchAPICall();



        gamesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Game game = (Game) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(ViewGames.this, Game_Details.class);
                if (game.getPegi() != null)
                {
                    intent.putExtra("GameData",game);
                    intent.putExtra("GamePegiData", game.getPegi());
                }
                else
                {
                    intent.putExtra("GameData",game);
                }

                startActivity(intent);

            }
        });

    }


    private void gameAPICall()
    {
        String apiSite = "https://api-endpoint.igdb.com/"; //sets the API site string.

        OkHttpClient.Builder GamesHttp = new OkHttpClient.Builder(); //Creates a new HTTP Request Builder
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(apiSite).addConverterFactory(GsonConverterFactory.create()); //Creates a new retrofit builder, sets the API string as the baseURL and sets GSON as the converter factory.
        final Retrofit GameFit = builder.client(GamesHttp.build()).build(); //Builds builder and fitted with the HTTPClient builder.

        final GameInterface GameInt = GameFit.create(GameInterface.class); //Uses the GameInterface java class to create an interface.
        Call<List<Game>> GameAPICall = GameInt.getGames(); //creates a call object using builder that was created and the getGames method in GameInterface class.


        GameAPICall.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> GameAPICall, Response<List<Game>> response)
            {
                List<Game> games = response.body(); //Stores the list of games in the games variable
                gameAdapter = new Game_Adapter(ViewGames.this, games); //Creates/applies the Game_Adapter class to the data which filters and formats the data.
                gamesList.setAdapter(gameAdapter); //Sets the adapter to the ListView so that the formatted data can be parsed to it.
                Log.d(TAG,"Games found: " + games.size()); //Displays number of games found in the log.


                for (int g = 0; g < games.size(); g++) //Iterates though each game object within games list
                {
                    String coverURL = games.get(g).getCover().getUrl(); //Gets URL for the cover image.
                    CoverURLS.add(coverURL); //Adds URL to thr coverURL list
                }

                Log.d(TAG,"covers found: " + CoverURLS.size());
            }

            @Override
            public void onFailure(Call<List<Game>> GameAPICall, Throwable t)
            {
                Log.e(TAG, "Error " + t); //Displays error in log if API call fails.
                Toast.makeText(ViewGames.this, "Error:(", Toast.LENGTH_SHORT).show(); //Popup message displayed if call fails.
            }

        });
    }


    private void gameSearchAPICall()
    {
        gameSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String s) {

                String query = gameSearch.getQuery().toString();
                String apiSite = "https://api-endpoint.igdb.com/"; //sets the API site string.
                OkHttpClient.Builder GamesHttp = new OkHttpClient.Builder(); //Creates a new HTTP Request Builder
                Retrofit.Builder builder = new Retrofit.Builder().baseUrl(apiSite).addConverterFactory(GsonConverterFactory.create()); //Creates a new retrofit builder, sets the API string as the baseURL and sets GSON as the converter factory.
                final Retrofit GameFit = builder.client(GamesHttp.build()).build(); //Builds builder and fitted with the HTTPClient builder.
                GameSearchInterface GameSearchInt = GameFit.create(GameSearchInterface.class);
                Call<List<Game>> GameSearchAPICall = GameSearchInt.getSearchGames(query);
                GameSearchAPICall.enqueue(new Callback<List<Game>>() {
                    @Override
                    public void onResponse(Call<List<Game>> GameSearchAPICall, Response<List<Game>> response)
                    {
                        List<Game> searchgames = response.body(); //Stores the list of games in the searchgames variable
                        gameAdapter = new Game_Adapter(ViewGames.this, searchgames); //Creates/applies the Game_Adapter class to the data which filters and formats the data.
                        gamesList.setAdapter(gameAdapter); //Sets the adapter to the ListView so that the formatted data can be parsed to it.

                        if (searchgames.size()>0) //Checks size of searched games to see how many results found to display correct message.
                            {
                            Toast.makeText(ViewGames.this, "Call Successful", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(ViewGames.this, "No Games Found Try Again", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<List<Game>> GameSearchAPICall, Throwable t)
                    {
                        Log.e(TAG,t.toString());

                    }
                });

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

    }

    //Creates option menu using the items within menu xml file/////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bookmark_only, menu);
        return super.onCreateOptionsMenu(menu);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Checks which menu option has been selected and says what to do////////////////////////////////
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.bookmarks:
                Intent intent = new Intent(ViewGames.this, Bookmark_Games.class);
                startActivity(intent);
                break;

        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////




    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }
}
