package com.example.aaronkalsi.video_game_dictionary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.aaronkalsi.video_game_dictionary.Interfaces.DeveloperInterface;
import com.example.aaronkalsi.video_game_dictionary.Interfaces.DeveloperSearchInterface;
import com.example.aaronkalsi.video_game_dictionary.Models.Developer;

import java.util.ArrayList;
import java.util.List;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Created by Aaron Kalsi on 15/03/2018.

public class ViewDevelopers extends AppCompatActivity {

    private static final String TAG = "debug";
    private ListView devsList; //ListView Variable
    private Developer_Adapter devAdapter; //game adapter variable
    private SearchView devSearch; //gameSearch Variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_developers);
        devsList = findViewById(R.id.DevelopersList); //Links the devList variable with the DevelopersList in the layout/view.
        devSearch = findViewById(R.id.DevSearchBar);  //Links the devSearch variable with the DevSearchBar in the layout/view.

        DevAPICall();
        DevSearchAPICall();


        devsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //finish();
                Developer developer = (Developer) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(ViewDevelopers.this, Developer_Details.class);
                intent.putExtra("DevData", developer);
                intent.putExtra("DevLogoData", developer.getLogo());
                startActivity(intent);
            }
        });
    }

    private void DevAPICall()
    {
        String apiSite = "https://api-endpoint.igdb.com/"; //sets the API site string.

        OkHttpClient.Builder GamesHttp = new OkHttpClient.Builder(); //Creates a new HTTP Request Builder
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(apiSite).addConverterFactory(GsonConverterFactory.create()); //Creates a new retrofit builder, sets the API string as the baseURL and sets GSON as the converter factory.
        final Retrofit DeveloperFit = builder.client(GamesHttp.build()).build(); //Builds builder and fitted with the HTTPClient builder.


        DeveloperInterface DevInt= DeveloperFit.create(DeveloperInterface.class); //Uses the DeveloperInterface java class to create an interface.
        Call<List<Developer>> DeveloperAPICall = DevInt.getDevelopers();

        DeveloperAPICall.enqueue(new Callback<List<Developer>>() {

            @Override
            public void onResponse(Call<List<Developer>> DeveloperAPICall, Response<List<Developer>> response) {
                List<Developer> developers = response.body(); //Stores the list of developers in the developers variable.
                devAdapter = new Developer_Adapter(ViewDevelopers.this, developers); //Creates/applies the Developer_Adapter class to the data which filters and formats the data.
                devsList.setAdapter(devAdapter); //Sets the adapter to the ListView so that the formatted data can be parsed to it.
                Log.d(TAG,"Developers found: " + developers.size()); //Displays number of developers found in the log.
            }

            @Override
            public void onFailure(Call<List<Developer>> DeveloperAPICall, Throwable t)
            {
                Log.e(TAG, "Error " + t); //Displays error in log if API call fails.
                Toast.makeText(ViewDevelopers.this, "Error:(", Toast.LENGTH_SHORT).show(); //Popup message displayed if call fails.

            }
        });
    }




    private void DevSearchAPICall()
    {
        devSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {

                String apiSite = "https://api-endpoint.igdb.com/"; //sets the API site string.
                OkHttpClient.Builder GamesHttp = new OkHttpClient.Builder(); //Creates a new HTTP Request Builder
                Retrofit.Builder builder = new Retrofit.Builder().baseUrl(apiSite).addConverterFactory(GsonConverterFactory.create()); //Creates a new retrofit builder, sets the API string as the baseURL and sets GSON as the converter factory.
                final Retrofit DeveloperFit = builder.client(GamesHttp.build()).build(); //Builds builder and fitted with the HTTPClient builder.

                String query = devSearch.getQuery().toString();
                DeveloperSearchInterface DevSearchInt = DeveloperFit.create(DeveloperSearchInterface.class);
                Call<List<Developer>> DevSearchAPICall = DevSearchInt.getSearchDevelopers(query);
                DevSearchAPICall.enqueue(new Callback<List<Developer>>() {
                    @Override
                    public void onResponse(Call<List<Developer>> DevSearchAPICall, Response<List<Developer>> response) {
                        List<Developer> searchdevelopers = response.body();//Stores the list of developers in the searchdevelopers variable.
                        devAdapter = new Developer_Adapter(ViewDevelopers.this, searchdevelopers); //Creates/applies the Developer_Adapter class to the data which filters and formats the data.
                        devsList.setAdapter(devAdapter); //Sets the adapter to the ListView so that the formatted data can be parsed to it.

                        if (searchdevelopers.size()>0) //Checks size of searched games to see how many results found to display correct message.
                        {
                            Toast.makeText(ViewDevelopers.this, "Call Successful", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(ViewDevelopers.this, "No Developers Found Try Again", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<List<Developer>> DevSearchAPICall, Throwable t) {

                        Log.e(TAG,t.toString());
                    }
                });

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }


    //Creates option menu using the items within menu xml file/////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bookmark_only, menu);
        return super.onCreateOptionsMenu(menu);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //Checks which menu option has been selected and says what to do////////////////////////////////
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.bookmarks:
                Intent intent = new Intent(ViewDevelopers.this, Bookmark_Developers.class);
                startActivity(intent);
                break;

        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void onBackPressed()
    {
        finish();//When back button pressed screen closes and moves to the previous screen.
    }
}
