package com.example.aaronkalsi.video_game_dictionary;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aaronkalsi.video_game_dictionary.Models.Game;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Game_Adapter extends ArrayAdapter<Game> {

    private static final String TAG = "Debug";

    public Game_Adapter(Context context, List<Game> games) {

        super(context, 0, games);

    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent){

        Game games = getItem(position);



        if(convertview==null)
        {
            convertview= LayoutInflater.from(getContext()).inflate(R.layout.game_list_view_layout,parent,false); // Uses game_list_view_layout to create layout object to use when data objects are passed through it (Formats Data).
        }
        TextView gameName = convertview.findViewById(R.id.GameName); //Assigns gameName to the text view in the layout for game list view.
        ImageView gameCover = (ImageView)convertview.findViewById(R.id.GameCoverImg); //Assigns gameCover to the image view in the list view format layout.

        gameName.setText(games.getName()); //Sets the text of the gameName in table view using data from games.getName.

        String mainCoverURL= "https:"+games.getCover().getUrl();
        games.setCoverURL(mainCoverURL);
        String coverURL = games.getCoverURL();
        Picasso.get().load(coverURL).fit().into(gameCover);
        Log.d(TAG, coverURL);

        return convertview;
    }
}
