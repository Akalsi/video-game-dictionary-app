package com.example.aaronkalsi.video_game_dictionary;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import com.example.aaronkalsi.video_game_dictionary.Models.Developer;
import com.squareup.picasso.Picasso;


//Created by Aaron Kalsi on 16/03/2018.


public class Developer_Adapter extends ArrayAdapter<Developer> {

    private static final String TAG = "Debug";

    public Developer_Adapter (Context context, List<Developer> developers)
    {

        super(context, 0, developers);
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent)
    {
        Developer developers = getItem(position);

        if(convertview==null) //Checks if the view is null.
        {
            convertview= LayoutInflater.from(getContext()).inflate(R.layout.developer_list_view_layout,parent,false); // Uses developer_list_view_layout to create layout object to use when data objects are passed through it (Formats Data).
        }

        TextView devName = convertview.findViewById(R.id.Developer_Name); //Assigns devName to the text view in the layout for developer list view.
        ImageView devLogo = convertview.findViewById(R.id.DevLogoImg); //Assigns devLogo to the image view in the list view format layout.

        devName.setText(developers.getName()); //Sets the text of the devNames in table view using data from developers.getName.

        if (developers.getLogo().getUrl() == null)
        {
            devLogo.setImageResource(R.mipmap.no_image_avaliable);
        }
        else
        {
            String mainLogoURL = "https:"+developers.getLogo().getUrl();
            developers.setLogoURL(mainLogoURL);
            String LogoURL =  developers.getLogoURL();
            Picasso.get().load(LogoURL).fit().into(devLogo);

            Log.d(TAG, LogoURL);
        }



        return convertview;
    }
}
