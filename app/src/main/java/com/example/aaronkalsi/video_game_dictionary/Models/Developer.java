package com.example.aaronkalsi.video_game_dictionary.Models;

//Created by Aaron Kalsi on 15/03/2018.

//Developer POJO class used to convert JSON string data into objects.


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Developer implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("logo")
    @Expose
    private Logo logo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("start_date")
    @Expose
    private long startDate;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("logoURL")
    @Expose
    private String logoURL;

    public Developer(int id, String name, String url, String description, long startDate, String website, String twitter, String logoURL) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.description = description;
        this.startDate = startDate;
        this.website = website;
        this.twitter = twitter;
        this.logoURL = logoURL;
    }

    public Developer(){};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public Developer (Parcel in) {

        int[] devdata1 = new int[1];
        String[] devdata2 = new String[6];
        long[] devdata3 = new long[1];

        in.readIntArray(devdata1);
        this.id = devdata1[0];

        in.readStringArray(devdata2);
        this.name = devdata2[0];
        this.url = devdata2[1];
        this.description = devdata2[2];
        this.website = devdata2[3];
        this.twitter = devdata2[4];
        this.logoURL =  devdata2[5];

        in.readLongArray(devdata3);
        this.startDate = devdata3[0];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeIntArray(new int[] {this.id});
        dest.writeStringArray(new String[] {this.name, this.url, this.description, this.website, this.twitter,this.logoURL});
        dest.writeLongArray(new long[] {this.startDate});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Developer createFromParcel (Parcel in) {return new Developer(in);}

        public Developer[] newArray(int size) {return new Developer[size];}
    };

}