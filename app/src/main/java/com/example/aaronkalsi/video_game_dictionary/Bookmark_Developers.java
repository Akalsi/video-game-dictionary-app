package com.example.aaronkalsi.video_game_dictionary;

//Created by Aaron Kalsi on 25/04/2018.

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.aaronkalsi.video_game_dictionary.Models.Developer;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class Bookmark_Developers extends AppCompatActivity{

    private ListView bookmarkedDevsListView;
    private DatabaseReference appDatabase;
    private static final String TAG = "debug";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookmarked_developers);

        bookmarkedDevsListView = findViewById(R.id.SavedDevsList);

        appDatabase = FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Developers"); //Sets FBDB reference

        getBookmarkedDevelopers();

        bookmarkedDevsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Developer developer = (Developer) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(Bookmark_Developers.this, Developer_Details.class);
                intent.putExtra("DevData",developer);
                startActivity(intent);
            }
        });
    }

    //Get the saved games from Fire Base DB using FBListadapter,setting layout xml file and linking adapter to list view
    private void getBookmarkedDevelopers()
    {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://video-game-dictionary-app.firebaseio.com/VideoGameDictionaryDatabase/Developers");

        FirebaseListOptions<Developer> options = new FirebaseListOptions.Builder<Developer>()
                .setQuery(dbRef, Developer.class) //Sets DBRef and class of objects in list
                .setLayout(R.layout.developer_list_view_layout) //Sets layout file of list view
                .build();

        FirebaseListAdapter<Developer> adapter= new FirebaseListAdapter<Developer>(options) {
            @Override
            protected void populateView(View v, Developer model, int position) {

                TextView DeveloperName = v.findViewById(R.id.Developer_Name);

                if (model.getName()!=null) // Checks if dev name is null
                {
                    DeveloperName.setText(model.getName());//Sets developer name TB text
                    Log.d(TAG, "Dev: "+model.getName());//Log message
                }
                else
                {
                    String string="This is NULL";
                    DeveloperName.setText(string);
                }



                ImageView DevLogo = v.findViewById(R.id.DevLogoImg);
                String logoURL = model.getLogoURL();
                Picasso.get().load(logoURL).fit().into(DevLogo); //Sets Dev LogoImg
            }
        };

        bookmarkedDevsListView.setAdapter(adapter); //Sets adapter for list view
        adapter.startListening(); //Start adapter

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }
}
