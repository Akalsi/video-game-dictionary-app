package com.example.aaronkalsi.video_game_dictionary.Models;

//Pegi POJO class used to convert JSON string data into objects.

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pegi implements Parcelable {

    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("rating")
    @Expose
    private int rating;

    public Pegi(String synopsis) {

        this.synopsis = synopsis;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public Pegi (Parcel in) {

        String[] pegidata1 = new String[1];
        int[] pegidata2 = new int[1];

        in.readStringArray(pegidata1);
        this.synopsis = pegidata1[0];

        in.readIntArray(pegidata2);
        this.rating = pegidata2[0];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeStringArray(new String[] {this.synopsis});
        dest.writeIntArray(new int[]{this.rating});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Pegi createFromParcel (Parcel in) {return new Pegi(in);}

        public Pegi[] newArray(int size) {return new Pegi[size];}
    };



}
