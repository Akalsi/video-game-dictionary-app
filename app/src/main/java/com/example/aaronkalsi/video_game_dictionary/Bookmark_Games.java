package com.example.aaronkalsi.video_game_dictionary;

//Created by Aaron Kalsi on 25/04/2018.

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.aaronkalsi.video_game_dictionary.Models.Game;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class Bookmark_Games extends AppCompatActivity{

    private ListView bookmarkedGamesListView;
    private DatabaseReference appDatabase;
    private static final String TAG = "debug";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookmarked_games);

        bookmarkedGamesListView = findViewById(R.id.SavedGamesList);

        appDatabase = FirebaseDatabase.getInstance().getReference("VideoGameDictionaryDatabase").child("Games"); //Sets FBDB reference

        getBookmarkedGames();

        bookmarkedGamesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Game game = (Game) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(Bookmark_Games.this, Game_Details.class);
                intent.putExtra("GameData",game);
                startActivity(intent);
            }
        });

    }

    //Get the saved games from Fire Base DB using FBListadapter,setting layout xml file and linking adapter to list view
    private void getBookmarkedGames()
    {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://video-game-dictionary-app.firebaseio.com/VideoGameDictionaryDatabase/Games");

        FirebaseListOptions<Game> options = new FirebaseListOptions.Builder<Game>()
                .setQuery(dbRef, Game.class) //sets DBRef and class of objects in list
                .setLayout(R.layout.game_list_view_layout) //Sets layout file of list view
                .build();

        FirebaseListAdapter<Game> adapter = new FirebaseListAdapter<Game>(options) {
            @Override
            protected void populateView(View v, Game model, int position) {

                TextView GameName = v.findViewById(R.id.GameName);
                GameName.setText(model.getName()); //sets game name TB
                Log.d(TAG, "Game: "+model.getName());//Log message

                ImageView GameCover = v.findViewById(R.id.GameCoverImg);

                String coverURL = model.getCoverURL();
                Picasso.get().load(coverURL).fit().into(GameCover); //Sets Game cover img

            }
        };

        bookmarkedGamesListView.setAdapter(adapter); //Sets adapter for list view
        adapter.startListening(); //Start adapter
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackPressed()
    {
        finish(); //When back button pressed screen closes and moves to the previous screen.
    }

}
