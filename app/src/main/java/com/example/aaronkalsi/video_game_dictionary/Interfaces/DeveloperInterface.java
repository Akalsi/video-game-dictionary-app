package com.example.aaronkalsi.video_game_dictionary.Interfaces;

//Created by Aaron Kalsi on 16/03/2018.

import com.example.aaronkalsi.video_game_dictionary.Models.Developer;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;


public interface DeveloperInterface {
    @Headers({"accept: application/json", "user-key: 34f109011f4a795335f4c81d296a5468"}) //Sets the headers needed for the API Call

    @GET("companies/?fields=name,url,logo,description,website,start_date,twitter&filter[twitter][exists]&filter[description][exists]&filter[logo][exists]") //Calls on the endpoint used for the API call this is added onto the baseURL.

    Call<List<Developer>> getDevelopers();
}
